"use strict";

// 1)
const name = "Anastasiia";
const admin = name;
console.log(admin);

// 2)
const days = 9;
const sec = days * 24 * 60 * 60;
console.log(`Seconds: ${sec}`);

// 3)
const value = prompt("Enter value...");
confirm(`Value: ${value}`);

